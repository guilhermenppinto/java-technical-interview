package com.surecloud.javatechnicalinterview;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages = "com.surecloud.javatechnicalinterview")
public class JavaTechnicalInterviewApplication {
	
	private static ApplicationContext applicationContext;

	public static void main(String[] args) {
		
		applicationContext = 
		          new AnnotationConfigApplicationContext(JavaTechnicalInterviewApplication.class);

        for (String beanName : applicationContext.getBeanDefinitionNames()) {
            System.out.println(beanName);
        }
        SpringApplication.run(JavaTechnicalInterviewApplication.class, args);
	}

}
