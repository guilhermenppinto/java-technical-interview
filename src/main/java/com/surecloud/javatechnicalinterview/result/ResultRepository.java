package com.surecloud.javatechnicalinterview.result;

import java.util.List;
import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;

public interface ResultRepository extends JpaRepository<Result, UUID>{

	List<Result> findAll();
	
	Result getById(UUID id);
	
	Result save(Result result);

}
