package com.surecloud.javatechnicalinterview.result;

import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ResultService {
	
	@Autowired
	ResultRepository resultRepository;

	public List<Result> getAllResults() {
		return resultRepository.findAll();
	}

	public Result getResult(UUID id) {
		return resultRepository.getById(id);
	}

	public Result addResult(Result result) {
		return resultRepository.save(result);
	}

}
