package com.surecloud.javatechnicalinterview.result;

import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;

@Controller
public class ResultController {
	
	@Autowired
	ResultService resultService;
	
	@GetMapping("/results")
	public ResponseEntity<List<Result>> getAllResults() {
		return new ResponseEntity<List<Result>>(resultService.getAllResults(), HttpStatus.OK);
	}
	
	@GetMapping("/result/{id}")
	public ResponseEntity<Result> getResult(@PathVariable(value = "id", required = true) UUID id) {
		return new ResponseEntity<Result>(resultService.getResult(id), HttpStatus.OK);
	}
	
	@PutMapping("result/add")
	public ResponseEntity<Result> addResult(@RequestBody Result result) {
		return new ResponseEntity<Result>(resultService.addResult(result), HttpStatus.OK);
	}
	

}
